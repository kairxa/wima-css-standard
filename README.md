# Writing CSS

Well, CSS sucks. It's up to us how to make it less sucks.

#### Basically, Conventions.

- Folder structure
- Naming convention
- CSS rules grouping

#### Folder Structure

We are going to follow The Sass Way (tm) folder structure - which is 7+1 style. With a little bit of addition, just in
case you want to make separate output css target.

```
|root/
|
|-- build/
    |
    |-- scss/
        |
        |-- base/
        |
        |-- components/
        |
        |-- layout/
        |
        |-- pages/
        |
        |-- themes/
        |
        |-- utils/
        |
        |-- vendors/
        |
        |-- _shame.scss
        |
        |-- app.scss
```

Here's the folder structure, arranged by loading order:

1. Vendors
    - Bootstrap, Foundation, jQuery UI; those kinds of things.
2. Base
    - Typography rules, reset rules (which usually already managed by bootstrap)
3. Utils
    - Helper classes, mixins, mixin variables
4. Layout
    - Footer, header, navigation
5. Components
    - Buttons, inputs, gallery of news / pictures; such things
6. Page
    - Specific page rules
            - Page background, page margin, specific rules for heading, etc.
7. Themes
    - Usually non existent
    - As the name suggest, this one is for sites with theme.
8. Shame
    - Bonus stage, yay
    - Not a folder, but a mere _shame.scss
    - So someone (preferably client) is being a jerk. You have no choice but to use hacks. Here's where to put your hack.

##### Multiple entry (and thus, output) points

Just throw the entry point scss name into the respective folders.

```
|root/
|
|-- build/
    |
    |-- scss/
        |
        |-- base/
        |
        |-- components/
        |
        |-- layout/
        |
        |-- pages/
        |
        |-- themes/
        |
        |-- utils/
        |
        |-- vendors/
        |
        |-- _shame.scss
        |
        |-- minisite.scss
        |
        |-- main.scss
        |
        |-- cms.scss
```

Normally we don't need this, but just in case you have severe OCD in unimportant things like I have, here you go.

#### Naming convention

[Trello CSS](https://gist.github.com/bobbygrace/9e961e8982f42eb91b80) is nice. Thanks to Rudi <rudi.hermanto@at.co.id> who found this.

tl;dr version:

- use namespacing .with-this-format
- **DO NOT NEST THINGS** unless Trello CSS says it's okay; look at my s3 / village / arena css structure to see how bad it is
- give proper prefix: .mod-, .js-, .u-, .m-

#### CSS rules grouping

##### What is this?

Take a look at this example:

```css
position: fixed;
bottom: 0;
right: 50%;
z-index: 5;
margin-right: -37.5px;
margin-bottom: 20px;
width: 75px;
cursor: pointer;
text-align: center;
visibility: collapse;
opacity: 0;
-webkit-transition: opacity 0.3s cubic-bezier(0.55, 0, 0.1, 1), visibility 0s linear 0.3s;
transition: opacity 0.3s cubic-bezier(0.55, 0, 0.1, 1), visibility 0s linear 0.3s;
```

It looks.. Messy, right? How about this:

```css
margin-right: -37.5px;
margin-bottom: 20px;
```
```css
width: 75px;
```
```css
position: fixed;
bottom: 0;
right: 50%;
z-index: 5;
```
```css
visibility: collapse;
opacity: 0;
```
```css
cursor: pointer;
text-align: center;
```
```css
-webkit-transition: opacity 0.3s cubic-bezier(0.55, 0, 0.1, 1), visibility 0s linear 0.3s;
transition: opacity 0.3s cubic-bezier(0.55, 0, 0.1, 1), visibility 0s linear 0.3s;
```

##### What does it mean?

Simple, you group the rules. Basically it goes from the outermost to the innermost visibility rules, while giving one empty line in between groups.
Looks easier on the eyes, ain't it?

Well, this one is kinda optional, but try to give it a shot for a while. Maybe you'll like it,
maybe you don't. Hopefully you'll like it though :)

### Bonus: NPM as a build tool

Reference: [Keith Cirkel's blog](blog.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/)

##### What the hell?

Nothing really. Just a way to challenge build tools status quo right now since Gulp was rejected (gulp is boring anyway), and Grunt has been the
de-facto for a long time for our build tools env.

##### How does it works?

```json
scripts: {
    "sass:dev": "node-sass --output-style expanded build/scss -o dist/assets/stylesheets",
    "sass:prod": "node-sass --output-style compressed build/scss -o dist/assets/stylesheets"
}
```
```
npm run sass:dev
```

##### Explain a bit

Piping globally installed npm modules to npm scripts. Well, limited to packages who has CLI modules. Wait.. Grunt and
Gulp are limited with this similar problem too, right? grunt-sass, grunt-uglifyjs, etc you name it.
We are at the mercy of package maintainers anyway :)

Personally I kinda found these are easier than making gruntfiles/gulpfiles, that's why I played a bit with this. I'll try
to use them for my personal play-doh.

##### How about watch? Live-reload?

```
npm install onreload live-reload --save-dev
```
```
scripts: {
    "watch:css": "onreload 'build/scss' -- npm run sass:dev,
    "livereload": "livereload --port 9091"
}
```

then add `<script src="http://localhost:9091/></script>` inside your html file. Or just use live-server from npm.